import React from 'react';
import {hot} from 'react-hot-loader';
import "./index.less";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.startTimer = this.startTimer.bind(this);
        this.setTimer = this.setTimer.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
        this.stopTimer = this.stopTimer.bind(this);

        this.state = {
            timeLeft: null,
            timer: null
        }
    }

    startTimer() {
        if (this.state.timeLeft === null) {
            return alert('enter value')
        }
        clearInterval(this.timer)

        this.timer = setInterval(() => {
            document.querySelector('.start-button').disabled = true;
            let count = this.state.timeLeft - 1
            if (count === 0) {
                clearInterval(this.timer)
            }

            this.setState({
                timeLeft: count,
            })
        }, 1000);

    }

    setTimer() {
        let input = document.querySelector('#input')
        let setBtn = document.querySelector('.btn-set')
        if (!input.value) {
            return alert('Please, enter a number')
        }
        setBtn.disabled = true;
        return this.setState({
            timeLeft: input.value
        }), input.value = null, input.disabled = true;

    }

    resetTimer() {
        clearInterval(this.timer)
        let input = document.querySelector('#input')
        let setBtn = document.querySelector('.btn-set')
        document.querySelector('.start-button').disabled = false;
        let startBtn = document.querySelector('.start-button')
        startBtn.innerHTML="Start";
        input.disabled = false;
        setBtn.disabled = false;
        this.setState({
            timeLeft: null,
            timer: null
        })

    }

    stopTimer() {
        clearInterval(this.timer)
        let stopBtn = document.querySelector('.stop-button')
        let startBtn = document.querySelector('.start-button')
        startBtn.innerHTML="Resume";
        startBtn.disabled = false;
    }

    render() {
        return (
            <div className="app-wrapper">
                <div className="caption">
                    <h1 className="caption__h1">Timer</h1>
                </div>
                <div className="input-wrapper">
                    <Input/>
                    <Button name="Set" action={this.setTimer} className="button btn-set"/>
                </div>
                <Display timeLeft={this.state.timeLeft}/>
                <div className="button-wrapper">
                    <Button name="Start" action={this.startTimer} className="button start-button"/>
                    <Button name="Stop" action={this.stopTimer} className="button stop-button"/>
                    <Button name="Reset" action={this.resetTimer} className="button"/>
                </div>

            </div>

        )
    }
}

class Button extends React.Component {

    render() {
        return (
            <button className={this.props.className} onClick={this.props.action}>{this.props.name}</button>
        )
    }
}

class Display extends React.Component {
    constructor(props) {
        super(props)
        this.formatTime = this.formatTime.bind(this)
    }

    formatTime(time) {
        let mins = time / 60;
        let sec = time % 60;
        sec = sec < 10 ? '0' + parseInt(sec) : parseInt(sec);

        return parseInt(mins) + ' : ' + sec;
    }

    render() {
        if (this.props.timeLeft === 0 || this.props.timeLeft === null) {
            return <h1>00 : 00</h1>
        }
        return (
            <div>
                <h1> {this.formatTime(this.props.timeLeft)} </h1>
                <h2>min sec</h2>
            </div>

        )
    }
}

class Input extends React.Component {
    constructor(props) {
        super(props)
        this.checkInput = this.checkInput.bind(this)
    }

    checkInput(e) {
        let banText = parseFloat(e.key);

        if (isNaN(banText)) {
            e.preventDefault();
        }
    }

    render() {
        return (
            <div className="input-wrapper">
                <label for="input">Set timer in seconds</label>
                <input id="input" onKeyPress={this.checkInput} autoComplete="off" className="timer-input"></input>
            </div>
        )
    }
}

export default hot(module)(App);